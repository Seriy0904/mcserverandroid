package net.dulatello08.mcservermobile;


import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class ServerCore extends Socket{
    Socket server = new Socket("google.com", 80, getAddress(), 0);

    public ServerCore() throws IOException {
    }

    protected InetAddress getAddress() throws UnknownHostException{
        return InetAddress.getLocalHost();
    }
    protected void testStart() throws IOException {
        SocketAddress endpoint = new InetSocketAddress("google.com",80);
        server.connect(endpoint);
        boolean isConnected = server.isConnected();
        Log.e("CONNECTED", Boolean.toString(isConnected));
    }
}
